package com.devcamp.splitstring.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class SplitStringController {
    @GetMapping("/split")
    public ArrayList<String> splitString(@RequestParam String input) {
        ArrayList<String> result = new ArrayList<>();

        String[] splitStringList = input.split(" ");
        for (String element : splitStringList) {
            result.add(element);
        }

        return result;
    }
}
